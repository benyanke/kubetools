---

stages:
  - build
  - package  
  - upload

include:
  # Include tag handling stages
  - project: 'benyanke/common-ci-configs'
    file: 'includes/common/tag-stages/include.yml'

  # Include deb uploader
  #- project: 'benyanke/common-ci-configs'
  #  file: 'includes/packaging/deb-upload/include.yml'

build.fetch_deps:
  stage: build
  image:
    name: ubuntu:latest
    entrypoint: [""]
  #variables:
  #  GIT_STRATEGY: none
  artifacts:
    expire_in: 1h
    paths:
    - "linux-bin/*"
    - "completion/*"
    - "*.deb"
    - "nfpm.yaml"
  script:
    - find
    - apt update -qqq && apt install wget jq curl -yqqq
    - mkdir linux-bin completion

    - |
        get_latest_github_release() {
          curl --silent "https://api.github.com/repos/$1/releases/latest" | # Get latest release from GitHub api
          grep '"tag_name":' |                                            # Get tag line
          sed -E 's/.*"([^"]+)".*/\1/'                                    # Pluck JSON value
        }

    # kubectl
    - wget -q -O linux-bin/kubectl "https://dl.k8s.io/release/$(curl -L -s https://dl.k8s.io/release/stable.txt)/bin/linux/amd64/kubectl" && chmod +x linux-bin/kubectl
    - linux-bin/kubectl completion bash > completion/kubetools-kubectl

    # helm
    - wget -q -O /dev/stdout "https://get.helm.sh/helm-v3.8.1-linux-amd64.tar.gz" | tar -zxvf - linux-amd64/helm && mv linux-amd64/helm linux-bin/helm && rm -rf linux-amd64 && chmod +x linux-bin/helm
    - linux-bin/helm completion bash > completion/kubetools-helm

    # k9s
    - wget -q -O /dev/stdout "https://github.com/derailed/k9s/releases/download/$(get_latest_github_release derailed/k9s)/k9s_Linux_x86_64.tar.gz" | tar -zxvf - k9s && mv k9s linux-bin/k9s && chmod +x linux-bin/k9s
    - linux-bin/k9s completion bash > completion/kubetools-k9s

    # kubeval
    - wget -q -O /dev/stdout "https://github.com/instrumenta/kubeval/releases/download/$(get_latest_github_release instrumenta/kubeval)/kubeval-linux-amd64.tar.gz" | tar -zxvf - kubeval && mv kubeval linux-bin/kubeval && chmod +x linux-bin/kubeval

    # kubectx, kubens
    - wget -q -O linux-bin/kubectx "https://github.com/ahmetb/kubectx/releases/download/v0.9.4/kubectx"
    - wget -q -O linux-bin/kubens "https://github.com/ahmetb/kubectx/releases/download/v0.9.4/kubens"

    # stern
    - wget -q -O linux-bin/stern "https://github.com/wercker/stern/releases/download/$(get_latest_github_release wercker/stern)/stern_linux_amd64" && chmod +x linux-bin/stern
    - linux-bin/stern --completion bash > completion/kubetools-stern

    # kubeseal (intentionally not getting latest, to lock)
    - wget -q -O /dev/stdout "https://github.com/bitnami-labs/sealed-secrets/releases/download/v0.17.3/kubeseal-0.17.3-linux-amd64.tar.gz" | tar -zxvf - kubeseal && mv kubeseal linux-bin/kubeseal

    # kube-capacity
    - export kc_latest=$(get_latest_github_release robscott/kube-capacity)
    - wget -q -O /dev/stdout "https://github.com/robscott/kube-capacity/releases/download/${kc_latest}/kube-capacity_${kc_latest:1}_Linux_x86_64.tar.gz" | tar -zxvf - kube-capacity && mv kube-capacity linux-bin/kube-capacity

    # yaml diff tool
    - export dyff_latest=$(get_latest_github_release homeport/dyff)
    - wget -q -O /dev/stdout "https://github.com/homeport/dyff/releases/download/${dyff_latest}/dyff_${dyff_latest:1}_linux_amd64.tar.gz"  | tar -zxvf - dyff && mv dyff linux-bin/dyff
    - linux-bin/dyff completion bash > completion/kubetools-dyff

    - cp -a scripts/* linux-bin/
    - find


package.deb:
  stage: package
  image:
    name: goreleaser/nfpm:latest
    entrypoint: [""]
  variables:
    GIT_STRATEGY: none
  artifacts:
    expire_in: 4h
    paths:
    - "${GO_BINARY_PREFIX}_*"
    - "*.deb"
    - "nfpm.yaml"
  script:
    - find

    # set version tag
    - |
        if [[ "${CI_COMMIT_TAG}" == "" ]] ; then
          export RELEASE_TAG="v0.0.1"
        else
          export RELEASE_TAG="${CI_COMMIT_TAG}";
        fi;
        echo "Setting release tag to ${RELEASE_TAG}";
        # add version tag if not provided ('tbd' will be replaced next line)
        grep -q "^version: " nfpm.yaml || echo "version: tbd" >> nfpm.yaml;
        # replace version tag in-place
        sed -i "s/version: .*/version: ${RELEASE_TAG}/g" nfpm.yaml;


    - |
        if [[ "${CI_COMMIT_BRANCH}" == "${CI_DEFAULT_BRANCH}" ]] ; then
          echo "You're on default branch";
        else
          echo "You're on branch ${CI_COMMIT_BRANCH}";
        fi
    # TODO - add tag generation for non-tags
    # TODO - add rc-tag routing
    # TODO - add fail if version is defined in the file

    # add some fields with defaults if not already defined in the file
    - 'grep -qE "^name: " nfpm.yaml || echo "name: \"${CI_PROJECT_NAME}\"">> nfpm.yaml'
    - 'grep -qE "^homepage: " nfpm.yaml || echo "homepage: \"${CI_PROJECT_URL}\"">> nfpm.yaml'
    - 'grep -qE "^description: " nfpm.yaml || echo "description: \"${CI_PROJECT_TITLE} - built from ${CI_PROJECT_URL}\"">> nfpm.yaml'
    - 'grep -qE "^arch: " nfpm.yaml || echo "arch: \"amd64\"">> nfpm.yaml'
    - 'grep -qE "^platform: " nfpm.yaml || echo "platform: \"linux\"">> nfpm.yaml'

    - 'cat nfpm.yaml | grep -ve "^#"'
    - nfpm package --packager deb
    - find

# TODO - add bits to update homebrew tap repo
