#!/usr/bin/env python3
# convert yaml to json

import yaml, json, sys
sys.stdout.write(json.dumps(yaml.safe_load(sys.stdin.read())))
