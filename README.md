# KubeTools

This package is a set of commonly used kubernetes client-side tools for administrators.

## Included tools

 - [kubectl](https://kubernetes.io/docs/tasks/tools/)
 - [helm](https://helm.sh/) - Package manager for Kubernetes
 - [k9s](https://k9scli.io/) - Kubernetes TUI
 - [kubeval](https://github.com/instrumenta/kubeval) - Validate kubernetes manifests
 - [kubectx](https://github.com/ahmetb/kubectx) - Kubernetes context manager for interacting with multiple clusters
 - [kubens](https://github.com/ahmetb/kubectx) - Namespace manager for easy namespace switching
 - [stern](https://github.com/wercker/stern) - Easy log viewing
 - json2yaml - Put json in `stdin`, and get yaml in `stdout` - helpful for scripts
 - yaml2json - Put yaml in `stdin`, and get json in `stdout` - helpful for scripts
 - [kubeseal](https://github.com/bitnami-labs/sealed-secrets) - Sealed Secrets encrypted secrets management
 - [kube-capacity](https://github.com/robscott/kube-capacity) - Cluster capacity visualizer
 - [topsecret](https://gitlab.com/benyanke/topsecret/) - A better user experience for Sealed Secrets
 - [dyff](https://github.com/homeport/dyff) - A yaml diff tool
 - and a ton of helpful bash aliases
 - kubectl-rebalance - a kubectl plugin which redeploys all HPAs to rebalance a cluster

## Installation

### Apt Repo

Install with apt by running the following:

```shell
echo "deb [arch=amd64] http://apt.yanke.io/repo/prod/ focal main" | sudo tee "/etc/apt/sources.list.d/yanke_repo.list"
wget -qO - https://apt.yanke.io/repo/pubkey.txt | sudo apt-key add -
```

Then, you can install the package:

```shell
sudo apt update
sudo apt install -y kubetools
```
